package com.example.graphql.graphql.resolvers;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example.graphql.graphql.models.SuperCharacter;
import com.example.graphql.graphql.repos.SuperCharacterRepo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Mutation implements GraphQLMutationResolver {
    private SuperCharacterRepo characterRepo;

    public Mutation(SuperCharacterRepo repo){
        this.characterRepo = repo;
    }

    public SuperCharacter addCharacter(String name, Integer age){
        return characterRepo.addCharacter(name,age);
    }
}
