package com.example.graphql.graphql.resolvers;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.graphql.graphql.models.SuperCharacter;
import com.example.graphql.graphql.repos.SuperCharacterRepo;
import lombok.extern.slf4j.Slf4j;
import java.util.List;

@Slf4j
public class Query implements GraphQLQueryResolver {
    private SuperCharacterRepo superCharacterRepo;

    public Query(SuperCharacterRepo repo)    {
        this.superCharacterRepo = repo;
    }

    public List<SuperCharacter> characters(){
        return superCharacterRepo.getCharacters();
    }

    public SuperCharacter characterById(Integer id){
        return superCharacterRepo.getCharacterById(id);
    }



}
