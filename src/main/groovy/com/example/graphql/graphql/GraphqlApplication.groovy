package com.example.graphql.graphql
import com.example.graphql.graphql.repos.SuperCharacterRepo
import com.example.graphql.graphql.resolvers.Mutation
import com.example.graphql.graphql.resolvers.Query
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class GraphqlApplication {
	@Autowired
	SuperCharacterRepo superCharacterRepo;

	@Bean
	public Query query(){
		return new Query(superCharacterRepo);
	}

	@Bean
	public Mutation mutation(){
		return new Mutation(superCharacterRepo);
	}
	static void main(String[] args) {
		SpringApplication.run(GraphqlApplication, args);
	}

}
