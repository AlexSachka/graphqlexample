package com.example.graphql.graphql.repos;
import com.example.graphql.graphql.models.SuperCharacter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class SuperCharacterRepo {
    private List<SuperCharacter> characters = new ArrayList<>();

    public SuperCharacterRepo() {
        seedCharacters();
    }

    private void seedCharacters(){
        SuperCharacter s1 = new SuperCharacter.Builder().
                setId(1).setName("Hillel").setAge(23).build();
        SuperCharacter s2 = new SuperCharacter.Builder().
                setId(2).setName("Hillel2").setAge(25).build();
       this.characters.add(s1);
       this.characters.add(s2);
    }

    public List<SuperCharacter> getCharacters(){
        return this.characters;
    }

    public SuperCharacter getCharacterById(Integer id){
        return characters.stream().filter(e -> e.getId().equals(id)).
                findFirst().get();
    }

    public SuperCharacter addCharacter(String name, Integer age){
        SuperCharacter character = new SuperCharacter.Builder().setId(characters.size()+1).
                setName(name).setAge(age).build();
        this.characters.add(character);
        return character;
    }
}
