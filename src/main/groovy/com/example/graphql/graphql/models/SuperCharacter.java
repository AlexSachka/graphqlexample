package com.example.graphql.graphql.models;
import lombok.Builder;
import lombok.Data;


public class SuperCharacter {
    private Integer id;
    private String name;
    private Integer age;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public SuperCharacter(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.age = builder.age;
    }

    public static class Builder{
        private Integer id;
        private String name;
        private Integer age;


        public Builder setId(Integer id){
            this.id = id;
            return this;
        }

        public Builder setName(String name){
            this.name = name;
            return this;
        }

        public Builder setAge(Integer age){
            this.age = age;
            return this;
        }

        public SuperCharacter build(){

            return new SuperCharacter(this);
        }
    }


}
